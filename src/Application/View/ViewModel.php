<?php declare(strict_types=1);

namespace Novuso\Common\Application\View;

/**
 * ViewModel is a representation of data for the presentation layer
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
class ViewModel
{
}
