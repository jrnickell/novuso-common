<?php declare(strict_types=1);

namespace Novuso\Common\Domain\Event;

use Novuso\Common\Domain\Model\UuidIdentifier;

/**
 * EventId is a unique identifier for a domain event
 *
 * @copyright Copyright (c) 2015, Novuso. <http://novuso.com>
 * @license   http://opensource.org/licenses/MIT The MIT License
 * @author    John Nickell <email@johnnickell.com>
 * @version   0.0.2
 */
final class EventId extends UuidIdentifier
{
}
